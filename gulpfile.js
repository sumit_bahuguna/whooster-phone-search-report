const gulp = require('gulp');
const nodeSass = require('node-sass');
const sass = require('gulp-sass')(nodeSass);
const browserSync = require('browser-sync').create();

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: './'
        },
        files: '**/*.html'
    })
})

gulp.task('sass', function() {
    return gulp.src('scss/style.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('watch', gulp.parallel(['sass', 'browserSync', function(done) {
    gulp.watch('scss/**/*.scss', gulp.series(['sass']));
    done();
}]))